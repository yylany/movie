/** axios:发送ajax的代码 **/
// 设置默认的访问地址
//axios.defaults.baseURL = 'http://localhost:8080';


// 注册
function register(param) {
    // 发送ajax请求，并将结果返回
    return axios.post("/api/user/register", param)
}

//登录
function login(param) {
    // 发送ajax请求，并将结果返回
    return axios.post("/api/user/login", param)
}

// 更新数据
function updataUser(param) {
    return axios.post("/api/user/updataUser", param)
}
