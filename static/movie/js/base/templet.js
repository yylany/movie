document.write("<meta charSet=\"UTF-8\">");
document.write("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
document.write("<meta name=\"renderer\" content=\"webkit\">");
document.write("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
document.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1 , user-scalable=no\">");
document.write("<title>微电影</title>");
document.write("<link rel=\"shortcut icon\" href=\"../admin/images/logo.png\">");
document.write("<link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">");
document.write("<link rel='stylesheet' href='css/animate.css'>");
document.write(" <link rel=\"stylesheet\" href=\"css/bootstrap-movie.css\">")

document.write("<script src='js/jquery.min.js'></script>");
document.write("<script src='js/bootstrap.min.js'></script>");
document.write("<script src='js/jquery.singlePageNav.min.js'></script>");
document.write("<script src='js/wow.min.js'></script>");
document.write("<script src='js/jquery.lazyload.min.js'></script>");
document.write("<script src='../static/js/vue.js'></script>");
document.write("<script src='../static/js/axios-min.js'></script>");
document.write("<script src='../static/js/jquery-cookie.js'></script>");
document.write("<script src='js/base/api.js'></script>")

// 设置头部
const com_head = {
    template: `
<!--导航-->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!--小屏幕导航按钮和logo-->
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.html" class="navbar-brand" style="width:250px;">
                <img src="../admin/images/logo.png" style="height:30px;">&nbsp;微电影
            </a>
        </div>
        <!--小屏幕导航按钮和logo-->
        <!--导航-->
        <div class="navbar-collapse collapse">
            <form class="navbar-form navbar-left" role="search" style="margin-top:18px;">
                <div class="form-group input-group">
                    <input type="text" class="form-control" placeholder="请输入电影名！">
                    <span class="input-group-btn">
                        <a class="btn btn-default" href="search.html"><span class="glyphicon glyphicon-search"></span>&nbsp;搜索</a>
                    </span>
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="curlink" href="index.html"><span class="glyphicon glyphicon-film"></span>&nbsp;电影</a>
                </li>
                <li v-if="name==null">
                    <a class="curlink" href="login.html"><span class="glyphicon glyphicon-log-in"></span>&nbsp;登录</a>
                </li>
                <li v-if="name==null">
                    <a class="curlink" href="register.html"><span class="glyphicon glyphicon-plus"></span>&nbsp;注册</a>
                </li>
                <li v-if="name!=null">
                    <a class="curlink" @click.stop="quit()"><span class="glyphicon glyphicon-log-out"></span>&nbsp;退出</a>
                </li>
                <li v-if="name!=null">
                    <a class="curlink" href="user.html"><span class="glyphicon glyphicon-user"></span>&nbsp;会员</a>
                </li>
            </ul>
        </div>
        <!--导航-->
    </div>
</nav>
<!--导航-->
  `,
    data() {
        return {
            name: ""
        }
    },
    created() {
        this.name = sessionStorage.getItem("name")
    },
    methods: {
        quit() {
            axios.get("/api/user/logout").then(rep => {
                sessionStorage.removeItem("user")
                sessionStorage.removeItem("name")
                this.name = null
            })
        }
    }
}
//设置底部
const com_foot = {
    template: `
    <!--底部-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    ©&nbsp;2017&nbsp;flaskmovie.imooc.com&nbsp;京ICP备 13046642号-2
                </p>
            </div>
        </div>
    </div>
</footer>

<!--底部-->
    `

}

