package main

import (
	_ "com/yylany/movie/conf"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}
