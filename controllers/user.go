package controllers

import (
	"com/yylany/movie/models"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	uuid "github.com/satori/go.uuid"
	"io/ioutil"
	"strings"
)

type UserController struct {
	beego.Controller
}

//登录
func (self *UserController) Login() {
	var (
		err error
	)
	// 校验数据
	user := models.User{}
	// 解析前端提供来的json，并将数据封装到 User里面
	if err = ParseFormData(self, &user); err != nil {
		self.returnData(models.NewRequest("400", "数据异常", nil))
		return
	}
	logs.Info("数据成功获取", user)

	//获取用户登录的方式
	valid := validation.Validation{}
	// 默认登录方式为 账户名
	filterName := "name"
	if valid.Email(user.Name, "Email").Ok {
		filterName = "email"
	} else if valid.Phone(user.Name, "Phone").Ok {
		filterName = "phone"
	}
	logs.Info("用户登录方式为：", filterName)
	o := orm.NewOrm()
	dbUser := models.User{}
	err = o.QueryTable("user").Filter(filterName, user.Name).One(&dbUser)
	if err == orm.ErrNoRows {
		logs.Info("账户不存在", user.Phone)
		self.returnData(models.NewRequest("400", "数据异常", nil))
		return
	} else {
		logs.Info("进入对比密码", user.Phone)
		// 判断密码是否相等
		if user.GetPwdHash() == dbUser.Pwd {
			logs.Info("登录成功", user.Phone)
			self.SetSession("user", dbUser)
			self.returnData(models.NewRequest("200", "登录成功", &dbUser))
			return
		} else {
			logs.Info("登录失败", user.Phone)
			self.returnData(models.NewRequest("400", "密码错误", nil))
			return
		}
	}

}

// 注册
func (self *UserController) Register() {
	var (
		err error
	)
	// 校验数据
	user := models.User{}
	flag := ParseFormDataAndValid(self, &user)
	if !flag {
		logs.Error("解析数据并校验数据失败")
		self.returnData(models.NewRequest("400", "数据异常", nil))
		return
	}

	u := models.User{}
	// 获取数据库对象
	ormobj := orm.NewOrm()
	err = ormobj.QueryTable("user").Filter("phone", user.Phone).One(&u)
	//查看是否已经注册
	if err != orm.ErrNoRows {
		logs.Info("已经存在有此数据", user.Phone)
		self.returnData(models.NewRequest("400", "数据异常", nil))
		return
	} else {
		// 密码加密
		user.Pwd = user.GetPwdHash()
		//添加用户的唯一标识
		u2, _ := uuid.NewV4()
		user.Uuid = u2.String()
		// 将数据插入到数据库
		if _, err = ormobj.Insert(&user); err != nil {
			logs.Error("添加用户失败", err)
			self.returnData(models.NewRequest("400", "数据异常", nil))
			return
		}
		logs.Info("添加", user.Phone, "用户成功")
		self.returnData(models.NewRequest("200", "成功", nil))
		return
	}
}

// 退出 登录
func (self *UserController) Logout() {
	logs.Info("退出登录")
	self.DelSession("user")
	self.returnData(models.NewRequest("200", "退出成功", nil))
}

// 更新数据
func (self *UserController) UpdataUser() {

	user, ok := self.GetSession("user").(models.User)
	if !ok {
		logs.Info("获取session数据失败")
		self.returnData(models.NewRequest("400", "请重新登录", nil))
		return
	} else {

		formUser := models.User{}
		formUser.Pwd = user.Pwd
		flag := ParseFormDataAndValid(self, &formUser)
		if !flag {
			logs.Info("解析from数据失败")
			self.returnData(models.NewRequest("400", "数据异常", nil))
			return
		}

		//说明账号信息相同
		formUser.Id = user.Id
		// 获取数据库对象
		ormobj := orm.NewOrm()
		ormobj.Update(&formUser)
		self.returnData(models.NewRequest("200", "更新成功", &formUser))
		logs.Info("更新数据成功")
	}

}

// 数据封装
func (self *UserController) returnData(request interface{}) {
	self.Data["json"] = request
	self.ServeJSON()
}

// 解析数据并校验。 user需要传指针对象过来
func ParseFormDataAndValid(self *UserController, user interface{}) bool {
	// 校验数据
	// 解析前端提供来的json，并将数据封装到 User里面
	ParseFormData(self, user)
	// 校验前端传过来的字段，
	valid := validation.Validation{}
	if !Valid(self, &valid, user) {
		return false
	}
	return true
}

// 解析 request 传过来的json数据，并且将数据封装熬 rep中
func ParseFormData(self *UserController, rep interface{}) error {
	data, err := ioutil.ReadAll(self.Ctx.Request.Body)
	if err != nil {
		logs.Error("读取错误")
		return err
	}
	dataStr := string(data)
	splitMapPab := strings.Split(dataStr[1:len(dataStr)-1], ",")
	for e := range splitMapPab {
		split := strings.Split(splitMapPab[e], ":")
		key := split[0][1 : len(split[0])-1]
		value := split[1][1 : len(split[1])-1]
		self.Ctx.Request.Form[key] = []string{value}
	}
	if err := self.ParseForm(rep); err != nil {
		logs.Error(err)
		return err
	}
	return nil

}

// 校验from数据
func Valid(self *UserController, valid *validation.Validation, data interface{}) bool {
	b, _ := valid.Valid(data)
	if !b {
		var str string = ""
		for _, err := range valid.Errors {
			str = fmt.Sprintln(str, err.Name, err.Message)
		}
		request := models.NewRequest("400", str, nil)
		self.Data["json"] = &request
		self.ServeJSON()
		return false
	}
	return true

}
