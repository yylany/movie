package models

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

// 用户表
type User struct {
	Id      int32     `orm:"pk;column(id);auto" form:"-" json:"-"`                                                // 编号
	Name    string    `orm:"column(name);size(100);unique" form:"name" valid:"Required;MaxSize(100)" json:"name"` //昵称
	Pwd     string    `orm:"column(pwd);size(100)" form:"password" valid:"Required;MaxSize(100)" json:"-"`        // 密码
	RePwd   string    `orm:"-" form:"repassword" json:"-"`                                                        // 确认密码
	Email   string    `orm:"column(email);size(100);unique" form:"email" valid:"Email;MaxSize(100)" json:"email"` // 邮箱
	Phone   string    `orm:"column(phone);size(11);unique"  form:"phone"  valid:"Mobile" json:"phone"`            // 手机号码
	Info    string    `orm:"column(info);type(text);null" form:"info" json:"info"`                                // 个性简介
	Face    string    `orm:"column(face);size(255);null" form:"-" json:"face"`                                    //头像
	Addtime time.Time `orm:"column(addtime);auto_now_add;type(datetime)" form:"-" json:"addtime"`                 //注册时间
	Uuid    string    `orm:"column(uuid)" form:"-" json:"uuid"`                                                   // 唯一标识符
}

func (u *User) TableName() string {
	return "user"
}

func (u *User) GetPwdHash() string {
	ctx := sha256.New()
	ctx.Write([]byte(u.Pwd))
	return hex.EncodeToString(ctx.Sum(nil))
}

// 会员登录日志
type Userlog struct {
	Id      int32     `orm:"pk;column(id)"`                               // 编号
	UserId  int32     `orm:"column(user_id)"`                             //所属会员
	Ip      string    `orm:"size(30);column(ip)"`                         //当前登录的id
	Area    string    `orm:"size(100);column(area)"`                      //地址
	Addtime time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //登录时间
}

func (u *Userlog) TableName() string {
	return "userlog"
}

//标签
type Tag struct {
	Id      int32     `orm:"pk;column(id)"`                               // 编号
	Name    string    `orm:"column(name);size(100)"`                      //标题
	Addtime time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //添加时间
}

func (t *Tag) TableName() string {
	return "tag"
}

//电影
type Movie struct {
	Id          int32     `orm:"ok;column(id)"`                               // 编号
	Title       string    `orm:"size(255);column(title)"`                     // 标题
	Url         string    `orm:"size(255);column(url)"`                       // 地址
	Info        string    `orm:"type(text);column(info)"`                     // 简介
	Logo        string    `orm:"size(255);column(logo)"`                      // 封面
	Star        int8      `orm:"column(star)"`                                //星级
	PlayNum     int64     `orm:"column(playNum)"`                             //播放量
	Commentnum  int64     `orm:"column(commentnum)"`                          //评论量
	TagId       int32     `orm:"column(tag_id)"`                              //所属标签id
	Area        string    `orm:"size(255);column(area)"`                      //上映地区
	ReleaseTime time.Time `orm:"column(release_time)"`                        //上映时间
	Length      string    `orm:"size(100);column(length)"`                    //播放时间
	Addtime     time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //添加时间
}

func (m *Movie) TableName() string {
	return "movie"
}

//预告
type Preview struct {
	Id      int32     `orm:"ok;column(id)"`                               // 编号
	Title   string    `orm:"size(255);column(title)"`                     // 标题
	Logo    string    `orm:"size(255);column(logo)"`                      // 封面
	Addtime time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //添加时间
}

func (p *Preview) TableName() string {
	return "preview"
}

// 评论
type Comment struct {
	Id      int32     `orm:"ok;column(id)"`                               // 编号
	Content string    `orm:"type(text);column(content)"`                  // 简介
	MovieId int32     `orm:"column(movie_id)"`                            //  所属电影
	UserId  int32     `orm:"column(user_id)"`                             //  所属用户
	Addtime time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //添加时间
}

func (c *Comment) TableName() string {
	return "comment"
}

// 电影收藏
type Moviecol struct {
	Id      int32     `orm:"ok;column(id)"`                               // 编号
	MovieId int32     `orm:"column(movie_id)"`                            //  所属电影
	UserId  int32     `orm:"column(user_id)"`                             //  所属用户
	Addtime time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //添加时间
}

func (m *Moviecol) TableName() string {
	return "moviecol"
}

//权限
type Auth struct {
	Id      int32     `orm:"ok;column(id)"`                               // 编号
	Name    string    `orm:"column(name);size(100)"`                      //名称
	Url     string    `orm:"size(255);column(url)"`                       // 地址
	Addtime time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //添加时间
}

func (a *Auth) TableName() string {
	return "auth"
}

// 角色
type Role struct {
	Id      int32     `orm:"ok;column(id)"`                               // 编号
	Name    string    `orm:"column(name);size(100)"`                      //名称
	Auths   string    `orm:"size(600);column(auths)"`                     // 地址
	Addtime time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //添加时间
}

func (r *Role) TableName() string {
	return "role"
}

// 管理员
type Admin struct {
	Id      int32     `orm:"ok;column(id)"`                               // 编号
	Name    string    `orm:"column(name);size(100)"`                      //管理员账号
	Pwd     string    `orm:"column(pwd);size(100)"`                       //管理员账号
	IsSuper int8      `orm:"column(is_super)"`                            //是否为超级管理员， 0 为超级管理员
	RoleId  int32     `orm:"column(role_id)"`                             // 所属角色
	Addtime time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //添加时间
}

func (a *Admin) TableName() string {
	return "admin"
}

//管理员登录日志
type Adminlog struct {
	Id       int32     `orm:"ok;column(id)"`                               // 编号
	Admin_id int32     `orm:"column(admin_id)"`                            //所属会员
	Ip       string    `orm:"size(30);column(ip)"`                         //当前登录的id
	Addtime  time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //添加时间
}

func (a *Adminlog) TableName() string {
	return "adminlog"
}

type Oplog struct {
	Id       int32     `orm:"ok;column(id)"`                               // 编号
	Admin_id int32     `orm:"column(admin_id)"`                            //所属会员
	Ip       string    `orm:"size(30);column(ip)"`                         //当前登录的id
	Reason   string    `orm:"column(reason);size(600)"`                    // 操作原因
	Addtime  time.Time `orm:"column(addtime);auto_now_add;type(datetime)"` //添加时间
}

func (o *Oplog) TableName() string {
	return "oplog"
}

// 用于加载的时候初始化数据库
func init() {
	// 读取配置信息，获取数据库地址
	dbUser := beego.AppConfig.DefaultString("db_user", "root")
	dbPwd := beego.AppConfig.DefaultString("db_pwd", "")
	dbUrl := beego.AppConfig.DefaultString("db_url", "go_movie?charset=utf8")
	dateSource := fmt.Sprintf("%s:%s@/%s", dbUser, dbPwd, dbUrl)

	// 需要在init中注册定义的model
	orm.RegisterModel(new(User), new(Userlog), new(Oplog),
		new(Tag), new(Movie), new(Preview),
		new(Comment), new(Moviecol), new(Auth),
		new(Role), new(Admin), new(Adminlog))
	// 注册驱动
	orm.RegisterDriver("mysql", orm.DRMySQL)
	//数据库地址
	orm.RegisterDataBase("default", "mysql", dateSource)
	// 根据 注册的model生成表
	orm.RunSyncdb("default", false, true)
}
