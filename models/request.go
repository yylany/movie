package models

// 用于前后台分离的数据响应
type Request struct {
	Code string      `json:"code"` // 响应码值
	Msg  string      `json:"msg"`  // 响应信息
	Data interface{} `json:"data"` // 响应数据
}

func NewRequest(code, msg string, data interface{}) *Request {
	obj := new(Request)
	obj.Data = data
	obj.Code = code
	obj.Msg = msg
	return obj
}
