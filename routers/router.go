package routers

import (
	"com/yylany/movie/controllers"
	"github.com/astaxie/beego"
)

func init() {
	// 登录
	beego.Router("/api/user/login", &controllers.UserController{}, "post:Login")
	// 注册
	beego.Router("/api/user/register", &controllers.UserController{}, "post:Register")
	// 退出登录
	beego.Router("/api/user/logout", &controllers.UserController{}, "get:Logout")
	// 更新用户数据
	beego.Router("/api/user/updataUser", &controllers.UserController{}, "post:UpdataUser")

}
