package conf

import (
	_ "com/yylany/movie/models"
	_ "com/yylany/movie/routers"
	"github.com/astaxie/beego"
)

// 初始化所有配置
func init() {
	// 设置首页地址
	beego.SetStaticPath("/", "static/movie/")
	// 设置 后台的地址
	beego.SetStaticPath("/admin", "static/admin/")
	// 设置 开发文档地址
	beego.SetStaticPath("/doc", "static/doc/")
	// 设置静态文件路径
	beego.SetStaticPath("/static", "static/static/")
	// 关闭模板渲染
	beego.BConfig.WebConfig.AutoRender = false

	beego.BConfig.WebConfig.EnableXSRF = false

}
